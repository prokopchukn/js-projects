import React from 'react';
import TodoBoardsCreator from '../TodoBoardsCreator';
import TodoNewBoard from '../TodoNewBoard';
import request from '../../request';
import './style.scss';

class Home extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      todoBoards: [],
    };
  }

  async componentDidMount() {
    try {
      const { data } = await request.get('/boards');

      this.setState({
        todoBoards: data,
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  }

  addNewBoard = async (titleBoard) => {
    try {
      const { todoBoards } = this.state;

      const { data } = await request.post('/boards', {
        title: titleBoard,
      });

      this.setState({
        todoBoards: [...todoBoards, {
          id: data.id,
          title: data.title,
        }],
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  removeBoard = async (idBoard) => {
    try {
      const { todoBoards } = this.state;

      await request.delete(`/boards/${idBoard}`);

      this.setState({
        todoBoards: todoBoards.filter((todoBoard) => todoBoard.id !== idBoard),
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  changeBoardTitle = async (idBoard, titleBoard) => {
    try {
      const { todoBoards } = this.state;

      await request.patch(`/boards/${idBoard}`, {
        title: titleBoard,
      });

      this.setState({
        todoBoards: todoBoards.map((todoBoard) => {
          if (todoBoard.id === idBoard) {
            return { id: todoBoard.id, title: titleBoard };
          }

          return todoBoard;
        }),
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  render() {
    const {
      todoBoards,
    } = this.state;

    return (
      <div className="home-container">
        <h1>To do boards</h1>
        <div className="container-boards">
          {
            todoBoards.map((todoBoard) => (
              <TodoNewBoard
                key={todoBoard.id}
                id={todoBoard.id}
                todoBoard={todoBoard.title}
                removeBoard={this.removeBoard}
                changeBoardTitle={this.changeBoardTitle}
              />

            ))
            }
          <TodoBoardsCreator
            addNewBoard={this.addNewBoard}
          />
        </div>
      </div>
    );
  }
}

export default Home;
