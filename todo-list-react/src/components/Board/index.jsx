import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import request from '../../request';

import TodoNewColumn from '../TodoNewColumn';
import TodoColumnCreator from '../TodoColumnCreator';

import './style.scss';
import withRouter from '../../withRouter';

class Board extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todoColumns: [],
    };
  }

  async componentDidMount() {
    try {
      const { params } = this.props;
      const { data } = await request.get(`/boards/${params.id}/todo-columns`);

      this.setState({
        todoColumns: data,
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  }

  addNewColumn = async (fieldValue) => {
    try {
      const { params } = this.props;
      console.log('🚀 ~ file: index.jsx ~ line 35 ~ Board ~ addNewColumn= ~ params', params);
      const { todoColumns } = this.state;

      const { data } = await request.post('/todo-columns', {
        title: fieldValue,
        boardId: params.id,
      });

      this.setState({
        todoColumns: [...todoColumns, {
          id: data.id,
          title: data.title,
        }],
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  changeColumnTitle = async (idColumn, fieldValue) => {
    try {
      const { todoColumns } = this.state;

      await request.patch(`/todo-columns/${idColumn}`, {
        title: fieldValue,
      });

      this.setState({
        todoColumns: todoColumns.map((todoColumn) => {
          if (todoColumn.id === idColumn) {
            return { id: todoColumn.id, title: fieldValue };
          }

          return todoColumn;
        }),
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  removeColumn = async (idColumn) => {
    try {
      const { todoColumns } = this.state;

      await request.delete(`/todo-columns/${idColumn}`);

      this.setState({
        todoColumns: todoColumns.filter((todoColumn) => todoColumn.id !== idColumn),
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  render() {
    const {
      todoColumns,
    } = this.state;

    return (
      <>
        <div className="return-home">
          <Link to="/">Home</Link>
        </div>

        <div className="todo-column-list">
          {
            todoColumns.map((todoColumn) => (
              <TodoNewColumn
                key={todoColumn.id}
                id={todoColumn.id}
                changeColumnTitle={this.changeColumnTitle}
                todoColumn={todoColumn.title}
                removeColumn={this.removeColumn}
                buttonTitle="+ Add a card"
              />
            ))
          }
          <TodoColumnCreator
            onSubmit={this.addNewColumn}
          />
        </div>
      </>
    );
  }
}

Board.propTypes = {
  params: PropTypes.object,
};

export default withRouter(Board);
