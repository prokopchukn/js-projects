import React from 'react';
import PropTypes from 'prop-types';
import ActionButton from '../common/ActionButton';
import './style.scss';

class TodoFieldBoard extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      editField: '',
    };
  }

  componentDidMount() {
    const {
      additionalTitle = '',
    } = this.props;

    this.setState({
      editField: additionalTitle,
    });
  }

  onChange = (e) => {
    this.setState({
      editField: e.target.value,
    });
  };

  changeBoardTitleWrapper = () => {
    const { editField } = this.state;
    const { id, changeBoardTitle, changeFocus } = this.props;

    const trimmedValue = editField.trim();

    if (trimmedValue.length <= 100 && trimmedValue) {
      changeBoardTitle(id, editField);

      changeFocus();
    }
  };

  render() {
    const {
      editField,
    } = this.state;

    const {
      switchMode,
    } = this.props;

    return (
      <div
        className="container-edit"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <input
          type="text"
          value={editField}
          onChange={this.onChange}
        />
        <ActionButton
          title="Save"
          className="change-title"
          onClick={this.changeBoardTitleWrapper}
        />
        <ActionButton
          title="Back"
          className="back"
          onClick={switchMode}
        />
      </div>
    );
  }
}

TodoFieldBoard.propTypes = {
  switchMode: PropTypes.func.isRequired,
  additionalTitle: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  changeBoardTitle: PropTypes.func.isRequired,
  changeFocus: PropTypes.func.isRequired,
};

export default TodoFieldBoard;
