import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class ActionButton extends React.PureComponent {
  render() {
    const {
      onClick,
      className,
      title,
    } = this.props;

    return (
      // eslint-disable-next-line jsx-a11y/control-has-associated-label
      <button
        type="button"
        onClick={onClick}
        className={className}
      >
        {title}
      </button>
    );
  }
}

ActionButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  title: PropTypes.string,
};

export default ActionButton;
