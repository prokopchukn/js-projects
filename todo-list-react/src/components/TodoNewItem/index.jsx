import React from 'react';
import PropTypes from 'prop-types';

import ActionButton from '../common/ActionButton';
import TodoField from '../TodoField';

import './style.scss';

class TodoNewItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      focused: false,
    };
  }

  removeItemWrapper = () => {
    const { id, removeItem } = this.props;
    removeItem(id);
  };

  changeItemTitleWrapper = (fieldValue) => {
    const { focused } = this.state;
    const {
      changeItemTitle,
      id,
    } = this.props;

    changeItemTitle(id, fieldValue);

    this.setState({
      focused: !focused,
    });
  };

  changeFocusItemTitle = () => {
    const { focused } = this.state;

    this.setState({
      focused: !focused,
    });
  };

  render() {
    const { focused } = this.state;
    const { todoItem } = this.props;

    return (
      <div>
        {
          focused ? (
            <TodoField
              title="Save"
              isTextarea
              switchMode={this.changeFocusItemTitle}
              additionalTitle={todoItem}
              onSubmit={this.changeItemTitleWrapper}
            />
          ) : (
            <div className="container-item">
              {todoItem}
              <div className="case-buttons">
                <ActionButton
                  className="edit-item"
                  onClick={this.changeFocusItemTitle}
                />
                <ActionButton
                  className="delete-item"
                  onClick={this.removeItemWrapper}
                />
              </div>
            </div>
          )
        }
      </div>
    );
  }
}

TodoNewItem.propTypes = {
  id: PropTypes.number.isRequired,
  todoItem: PropTypes.string,
  changeItemTitle: PropTypes.func.isRequired,
  removeItem: PropTypes.func.isRequired,
};

export default TodoNewItem;
