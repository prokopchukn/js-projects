import React from 'react';
import PropTypes from 'prop-types';

import ActionButton from '../common/ActionButton';

import './style.scss';

class TodoField extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fieldValue: '',
    };
  }

  componentDidMount() {
    const {
      additionalTitle = '',
    } = this.props;

    this.setState({
      fieldValue: additionalTitle,
    });
  }

  onChange = (e) => {
    this.setState({
      fieldValue: e.target.value,
    });
  };

  clickTodoButton = () => {
    const { fieldValue } = this.state;
    const { onSubmit } = this.props;

    const trimmedValue = fieldValue.trim();

    if (trimmedValue.length <= 100 && trimmedValue) {
      onSubmit(fieldValue);
      this.setState({
        fieldValue: '',
      });
    }
  };

  render() {
    const {
      fieldValue,
    } = this.state;

    const {
      title,
      switchMode,
      isTextarea,
    } = this.props;

    return (
      <div className="container">
        {
          !isTextarea ? (
            <input
              value={fieldValue}
              onChange={this.onChange}
              type="text"
              placeholder="Enter list title..."
            />
          ) : (
            <textarea
              value={fieldValue}
              onChange={this.onChange}
              placeholder="Enter a title for this card..."
            />
          )
        }
        <ActionButton
          onClick={this.clickTodoButton}
          className="confirm-add-list btn"
          title={title}
        />
        <ActionButton
          onClick={switchMode}
          className="back btn"
          title="Back"
        />
      </div>
    );
  }
}

TodoField.propTypes = {
  additionalTitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  switchMode: PropTypes.func.isRequired,
  isTextarea: PropTypes.bool,
};

export default TodoField;
