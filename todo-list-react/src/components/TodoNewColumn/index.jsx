import React from 'react';
import PropTypes from 'prop-types';
import request from '../../request';

import ActionButton from '../common/ActionButton';
import TodoField from '../TodoField';
import TodoNewItem from '../TodoNewItem';

import './style.scss';
import withRouter from '../../withRouter';

class TodoNewColumn extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      focused: false,
      clicked: false,
      todoItems: [],
    };
  }

  async componentDidMount() {
    try {
      const { id } = this.props;
      const { data } = await request.get(`/todo-columns/${id}/todo-items`);

      this.setState({
        todoItems: data,
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  }

  changeFocusColumnTitle = () => {
    const { focused } = this.state;

    this.setState({
      focused: !focused,
    });
  };

  changeColumnTitleWrapper = (columnTitle) => {
    const { focused } = this.state;
    const {
      changeColumnTitle,
      id,
    } = this.props;

    changeColumnTitle(id, columnTitle);

    this.setState({
      focused: !focused,
    });
  };

  removeColumnWrapper = () => {
    const { id, removeColumn } = this.props;
    removeColumn(id);
  };

  addNewItem = () => {
    const { clicked } = this.state;

    this.setState({
      clicked: !clicked,
    });
  };

  addNewItems = async (fieldValue) => {
    try {
      const { todoItems } = this.state;
      const { id } = this.props;

      const { data } = await request.post('/todo-items', {
        title: fieldValue,
        'todo-columnId': id,
      });

      this.setState({
        todoItems: [...todoItems, {
          id: data.id,
          title: data.title,
        }],
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  removeItem = async (idItem) => {
    try {
      const { todoItems } = this.state;

      await request.delete(`/todo-items/${idItem}`);

      this.setState({
        todoItems: todoItems.filter((todoItem) => todoItem.id !== idItem),
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  changeItemTitle = async (idItem, fieldValue) => {
    try {
      const { todoItems } = this.state;

      await request.patch(`/todo-items/${idItem}`, {
        title: fieldValue,
      });

      this.setState({
        todoItems: todoItems.map((todoItem) => {
          if (todoItem.id === idItem) {
            return { id: todoItem.id, title: fieldValue };
          }

          return todoItem;
        }),
      });
    } catch (err) {
      console.error('Error happened, details -> ', err);
    }
  };

  render() {
    const {
      focused,
      clicked,
      todoItems,
    } = this.state;
    const {
      todoColumn,
      buttonTitle,
    } = this.props;

    return (
      <div className="container todo-container-list">
        {focused ? (
          <TodoField
            title="Save"
            switchMode={this.changeFocusColumnTitle}
            onSubmit={this.changeColumnTitleWrapper}
            additionalTitle={todoColumn}
          />
        ) : (
          <>
            <div
              onDoubleClick={this.changeFocusColumnTitle}
              className="todo-column-title"
            >
              {todoColumn}
            </div>
            <ActionButton
              className="delete-column"
              onClick={this.removeColumnWrapper}
            />
          </>
        )}
        {
          todoItems.map((todoItem) => (
            <TodoNewItem
              key={todoItem.id}
              id={todoItem.id}
              todoItem={todoItem.title}
              removeItem={this.removeItem}
              changeItemTitle={this.changeItemTitle}
            />
          ))
        }
        {
          clicked ? (
            <TodoField
              title="Add card"
              isTextarea
              switchMode={this.addNewItem}
              onSubmit={this.addNewItems}
            />
          ) : (
            <div>
              <ActionButton
                className="todo-column-item"
                onClick={this.addNewItem}
                title={buttonTitle}
              />
            </div>
          )
        }
      </div>
    );
  }
}

TodoNewColumn.propTypes = {
  id: PropTypes.number.isRequired,
  removeColumn: PropTypes.func.isRequired,
  changeColumnTitle: PropTypes.func.isRequired,
  todoColumn: PropTypes.string.isRequired,
  buttonTitle: PropTypes.string.isRequired,
};

export default withRouter(TodoNewColumn);
