import React from 'react';
import PropTypes from 'prop-types';

import ActionButton from '../common/ActionButton';
import TodoField from '../TodoField';

class TodoColumnCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hidden: false,
    };
  }

  switchMode = () => {
    const { hidden } = this.state;
    this.setState({
      hidden: !hidden,
    });
  };

  render() {
    const { hidden } = this.state;
    const { onSubmit } = this.props;

    return (
      <div>
        {!hidden ? (
          <ActionButton
            onClick={this.switchMode}
            className="todo-column-item"
            title="+ Add a list"
          />
        ) : (
          <TodoField
            onSubmit={onSubmit}
            switchMode={this.switchMode}
            title="Add list"
          />
        )}
      </div>
    );
  }
}

TodoColumnCreator.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default TodoColumnCreator;
