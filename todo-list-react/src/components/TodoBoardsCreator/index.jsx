import React from 'react';
import PropTypes from 'prop-types';
import ActionButton from '../common/ActionButton';
import './style.scss';

class TodoBoardsCreator extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      focused: false,
      titleBoard: '',
    };
  }

  onChange = (e) => {
    this.setState({
      titleBoard: e.target.value,
    });
  };

  changeFocus = () => {
    const { focused } = this.state;

    this.setState({
      focused: !focused,
    });
  };

  addNewBoardWrapper = () => {
    const {
      titleBoard,
    } = this.state;

    const {
      addNewBoard,
    } = this.props;

    const trimmedValue = titleBoard.trim();

    if (trimmedValue.length <= 100 && trimmedValue) {
      addNewBoard(titleBoard);

      this.setState({
        titleBoard: '',
      });

      this.changeFocus();
    }
  };

  render() {
    const {
      focused,
      titleBoard,
    } = this.state;

    return (
      <div>
        <div
          className="create-board"
          onClick={this.changeFocus}
        >
          <div>Create new Board</div>
        </div>
        {
          focused ? (
            <div className="title-board">
              <div>Board title:</div>
              <input
                value={titleBoard}
                onChange={this.onChange}
                placeholder="Enter the title..."
              />
              <ActionButton
                className="create"
                onClick={this.addNewBoardWrapper}
                title="Create"
              />
            </div>
          ) : null
        }
      </div>
    );
  }
}

TodoBoardsCreator.propTypes = {
  addNewBoard: PropTypes.func.isRequired,
};

export default TodoBoardsCreator;
