import React from 'react';
import PropTypes from 'prop-types';
import withRouter from '../../withRouter';
import ActionButton from '../common/ActionButton';
import TodoFieldBoard from '../TodoFieldBoard';
import './style.scss';

class TodoNewBoard extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      focused: false,
    };
  }

  removeBoardWrapper = (e) => {
    const { id, removeBoard } = this.props;

    e.stopPropagation();

    removeBoard(id);
  };

  changeFocus = (e) => {
    const { focused } = this.state;

    this.setState({
      focused: !focused,
    });
    e?.stopPropagation();
  };

  navigateToBoard = () => {
    const { navigate, id } = this.props;
    navigate(`/board/${id}`);
  };

  render() {
    const { focused } = this.state;
    const { todoBoard, changeBoardTitle, id } = this.props;

    return (
      <div
        className="board"
        onClick={this.navigateToBoard}
      >
        <div>{todoBoard}</div>
        <div className="case-button">
          <ActionButton
            className="change-title"
            onClick={this.changeFocus}
          />
          <ActionButton
            className="delete-board"
            onClick={this.removeBoardWrapper}
          />
        </div>
        {
          focused ? (
            <TodoFieldBoard
              additionalTitle={todoBoard}
              switchMode={this.changeFocus}
              changeBoardTitle={changeBoardTitle}
              id={id}
              changeFocus={this.changeFocus}
            />

          ) : null
        }
      </div>
    );
  }
}

TodoNewBoard.propTypes = {
  todoBoard: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  removeBoard: PropTypes.func.isRequired,
  changeBoardTitle: PropTypes.func.isRequired,
  navigate: PropTypes.func.isRequired,
};

export default withRouter(TodoNewBoard);
