import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';

function withRouter(Component) {
  function Wrapper(props) {
    const navigate = useNavigate();
    const params = useParams();

    // eslint-disable-next-line react/jsx-props-no-spreading
    return <Component params={params} navigate={navigate} {...props} />;
  }

  return Wrapper;
}

export default withRouter;
