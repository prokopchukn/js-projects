import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import boardReducer from './reducers/boardReducer';
import columnReducer from './reducers/columnReducer';
import itemReducer from './reducers/itemReducer';

const rootReducer = combineReducers({
  boardReducer,
  columnReducer,
  itemReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

export default store;
