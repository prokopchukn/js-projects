import request from '../../request';
import {
  getItemsAction,
  addItemAction,
  changeItemAction,
  removeItemAction,
} from '../actions/itemActions';

export const fetchItems = () => async (dispatch) => {
  try {
    const { data } = await request.get('/todo-items');

    dispatch(getItemsAction(data));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const createItems = (fieldValue, id) => async (dispatch) => {
  try {
    const { data } = await request.post('/todo-items', {
      title: fieldValue,
      'todo-columnId': id,
    });

    dispatch(addItemAction({
      id: data.id,
      title: data.title,
      'todo-columnId': id,
    }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const deleteItems = (idItem) => async (dispatch) => {
  try {
    await request.delete(`/todo-items/${idItem}`);

    dispatch(removeItemAction({ idItem }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const updateItems = (idItem, fieldValue) => async (dispatch) => {
  try {
    await request.patch(`/todo-items/${idItem}`, {
      title: fieldValue,
    });

    dispatch(changeItemAction({ idItem, fieldValue }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};
