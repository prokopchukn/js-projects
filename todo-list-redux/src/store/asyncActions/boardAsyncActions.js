import request from '../../request';
import {
  getBoardsAction,
  addBoardAction,
  changeBoardAction,
  removeBoardAction,
} from '../actions/boardActions';

export const fetchBoards = () => async (dispatch) => {
  try {
    const { data } = await request.get('/boards');

    dispatch(getBoardsAction(data));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const createBoards = (titleBoard) => async (dispatch) => {
  try {
    const { data } = await request.post('/boards', {
      title: titleBoard,
    });

    dispatch(addBoardAction({
      id: data.id,
      title: data.title,
    }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const deleteBoards = (idBoard) => async (dispatch) => {
  try {
    await request.delete(`/boards/${idBoard}`);

    dispatch(removeBoardAction({ idBoard }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const updateBoards = (idBoard, titleBoard) => async (dispatch) => {
  try {
    await request.patch(`/boards/${idBoard}`, {
      title: titleBoard,
    });

    dispatch(changeBoardAction({ idBoard, titleBoard }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};
