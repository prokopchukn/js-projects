import request from '../../request';
import {
  getColumnsAction,
  addColumnAction,
  changeColumnAction,
  removeColumnAction,
} from '../actions/columnActions';

export const fetchColumns = (params) => async (dispatch) => {
  try {
    const { data } = await request.get(`/boards/${params.id}/todo-columns`);

    dispatch(getColumnsAction(data));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const createColumns = (fieldValue, params) => async (dispatch) => {
  try {
    const { data } = await request.post('/todo-columns', {
      title: fieldValue,
      boardId: params.id,
    });

    dispatch(addColumnAction({ id: data.id, title: data.title }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const deleteColumns = (idColumn) => async (dispatch) => {
  try {
    await request.delete(`/todo-columns/${idColumn}`);

    dispatch(removeColumnAction({ idColumn }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};

export const updateColumns = (idColumn, fieldValue) => async (dispatch) => {
  try {
    await request.patch(`/todo-columns/${idColumn}`, {
      title: fieldValue,
    });

    dispatch(changeColumnAction({ idColumn, fieldValue }));
  } catch (err) {
    console.error('Error happened, details -> ', err);
  }
};
