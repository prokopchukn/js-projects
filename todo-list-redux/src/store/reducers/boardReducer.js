import { BoardTypes } from '../actions/boardActions';

const defaultState = {
  todoBoards: [],
};

const boardReducer = (state = defaultState, action) => {
  switch (action.type) {
    case BoardTypes.ADD_BOARD:
      return {
        ...state,
        todoBoards: [...state.todoBoards, action.payload],
      };
    case BoardTypes.GET_BOARDS:
      return {
        ...state, todoBoards: action.payload,
      };
    case BoardTypes.CHANGE_BOARD:
      return {
        ...state,
        todoBoards: state.todoBoards.map((todoBoard) => (
          todoBoard.id === action.payload.idBoard
            ? { ...todoBoard, title: action.payload.titleBoard }
            : todoBoard
        )),
      };
    case BoardTypes.REMOVE_BOARD:
      return {
        ...state,
        todoBoards: state.todoBoards.filter((todoBoard) => todoBoard.id !== action.payload.idBoard),
      };
    default:
      return state;
  }
};

export default boardReducer;
