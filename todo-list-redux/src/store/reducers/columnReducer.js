import { ColumnTypes } from '../actions/columnActions';

const defaultState = {
  todoColumns: [],
};

const columnReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ColumnTypes.ADD_COLUMN:
      return {
        ...state,
        todoColumns: [...state.todoColumns, action.payload],
      };
    case ColumnTypes.GET_COLUMNS:
      return {
        ...state,
        todoColumns: action.payload,
      };
    case ColumnTypes.CHANGE_COLUMN:
      return {
        ...state,
        todoColumns: state.todoColumns.map((todoColumn) => (
          todoColumn.id === action.payload.idColumn
            ? { ...todoColumn, title: action.payload.fieldValue }
            : todoColumn
        )),
      };
    case ColumnTypes.REMOVE_COLUMN:
      return {
        ...state,
        todoColumns: state.todoColumns.filter((todoColumn) => (
          todoColumn.id !== action.payload.idColumn
        )),
      };
    default:
      return state;
  }
};

export default columnReducer;
