import { ItemTypes } from '../actions/itemActions';

const defaultState = {
  todoItems: [],
};

const itemReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ItemTypes.ADD_ITEM:
      return {
        ...state, todoItems: [...state.todoItems, action.payload],
      };
    case ItemTypes.GET_ITEMS:
      return {
        ...state, todoItems: action.payload,
      };
    case ItemTypes.CHANGE_ITEM:
      return {
        ...state,
        todoItems: state.todoItems.map((todoItem) => (
          todoItem.id === action.payload.idItem
            ? { ...todoItem, title: action.payload.fieldValue }
            : todoItem
        )),
      };
    case ItemTypes.REMOVE_ITEM:
      return {
        ...state,
        todoItems: state.todoItems.filter((todoItem) => todoItem.id !== action.payload.idItem),
      };
    default:
      return state;
  }
};

export default itemReducer;
