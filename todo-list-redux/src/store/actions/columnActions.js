export const ColumnTypes = {
  ADD_COLUMN: 'ADD_COLUMN',
  GET_COLUMNS: 'GET_COLUMNS',
  CHANGE_COLUMN: 'CHANGE_COLUMN',
  REMOVE_COLUMN: 'REMOVE_COLUMN',
};

export const addColumnAction = (payload) => ({
  type: ColumnTypes.ADD_COLUMN,
  payload,
});

export const getColumnsAction = (payload) => ({
  type: ColumnTypes.GET_COLUMNS,
  payload,
});

export const changeColumnAction = (payload) => ({
  type: ColumnTypes.CHANGE_COLUMN,
  payload,
});

export const removeColumnAction = (payload) => ({
  type: ColumnTypes.REMOVE_COLUMN,
  payload,
});
