export const BoardTypes = {
  ADD_BOARD: 'ADD_BOARD',
  GET_BOARDS: 'GET_BOARDS',
  CHANGE_BOARD: 'CHANGE_BOARD',
  REMOVE_BOARD: 'REMOVE_BOARD',
};

export const addBoardAction = (payload) => ({
  type: BoardTypes.ADD_BOARD,
  payload,
});

export const getBoardsAction = (payload) => ({
  type: BoardTypes.GET_BOARDS,
  payload,
});

export const changeBoardAction = (payload) => ({
  type: BoardTypes.CHANGE_BOARD,
  payload,
});

export const removeBoardAction = (payload) => ({
  type: BoardTypes.REMOVE_BOARD,
  payload,
});
