export const ItemTypes = {
  ADD_ITEM: 'ADD_ITEM',
  GET_ITEMS: 'GET_ITEMS',
  CHANGE_ITEM: 'CHANGE_ITEM',
  REMOVE_ITEM: 'REMOVE_ITEM',
};

export const addItemAction = (payload) => ({
  type: ItemTypes.ADD_ITEM,
  payload,
});

export const getItemsAction = (payload) => ({
  type: ItemTypes.GET_ITEMS,
  payload,
});

export const changeItemAction = (payload) => ({
  type: ItemTypes.CHANGE_ITEM,
  payload,
});

export const removeItemAction = (payload) => ({
  type: ItemTypes.REMOVE_ITEM,
  payload,
});
