import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ActionButton from '../common/ActionButton';
import './style.scss';

function TodoFieldBoard(props) {
  const [editField, setEditField] = useState('');

  const {
    id,
    changeBoardTitle,
    changeFocus,
    switchMode,
  } = props;

  const additionalTitleWrapper = () => {
    const {
      additionalTitle = '',
    } = props;

    setEditField(additionalTitle);
  };

  useEffect(() => {
    additionalTitleWrapper();
  }, []);

  const onChange = (e) => {
    setEditField(e.target.value);
  };

  const changeBoardTitleWrapper = () => {
    const trimmedValue = editField.trim();

    if (trimmedValue.length <= 100 && trimmedValue) {
      changeBoardTitle(id, editField);

      changeFocus();
    }
  };

  return (
    <div
      className="container-edit"
      onClick={(e) => {
        e.stopPropagation();
      }}
    >
      <input
        type="text"
        value={editField}
        onChange={onChange}
      />
      <ActionButton
        title="Save"
        className="change-title"
        onClick={changeBoardTitleWrapper}
      />
      <ActionButton
        title="Back"
        className="back"
        onClick={switchMode}
      />
    </div>
  );
}

TodoFieldBoard.propTypes = {
  switchMode: PropTypes.func.isRequired,
  additionalTitle: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  changeBoardTitle: PropTypes.func.isRequired,
  changeFocus: PropTypes.func.isRequired,
};

export default TodoFieldBoard;
