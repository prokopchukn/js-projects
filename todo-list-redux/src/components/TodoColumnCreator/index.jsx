import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ActionButton from '../common/ActionButton';
import TodoField from '../TodoField';

function TodoColumnCreator(props) {
  const [hidden, setHidden] = useState(false);
  const { onSubmit } = props;

  const switchMode = () => {
    setHidden(!hidden);
  };

  return (
    <div>
      {!hidden ? (
        <ActionButton
          onClick={switchMode}
          className="todo-column-item"
          title="+ Add a list"
        />
      ) : (
        <TodoField
          onSubmit={onSubmit}
          switchMode={switchMode}
          title="Add list"
        />
      )}
    </div>
  );
}

TodoColumnCreator.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default TodoColumnCreator;
