import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ActionButton from '../common/ActionButton';
import TodoField from '../TodoField';

import './style.scss';

function TodoNewItem(props) {
  const [focused, setFocused] = useState(false);
  const {
    todoItem,
    changeItemTitle,
    id,
    removeItem,
  } = props;

  const removeItemWrapper = () => {
    removeItem(id);
  };

  const changeItemTitleWrapper = (fieldValue) => {
    changeItemTitle(id, fieldValue);

    setFocused(!focused);
  };

  const changeFocusItemTitle = () => {
    setFocused(!focused);
  };

  return (
    <div>
      {
          focused ? (
            <TodoField
              title="Save"
              isTextarea
              switchMode={changeFocusItemTitle}
              additionalTitle={todoItem}
              onSubmit={changeItemTitleWrapper}
            />
          ) : (
            <div className="container-item">
              {todoItem}
              <div className="case-buttons">
                <ActionButton
                  className="edit-item"
                  onClick={changeFocusItemTitle}
                />
                <ActionButton
                  className="delete-item"
                  onClick={removeItemWrapper}
                />
              </div>
            </div>
          )
        }
    </div>
  );
}

TodoNewItem.propTypes = {
  id: PropTypes.number.isRequired,
  todoItem: PropTypes.string,
  changeItemTitle: PropTypes.func.isRequired,
  removeItem: PropTypes.func.isRequired,
};

export default TodoNewItem;
