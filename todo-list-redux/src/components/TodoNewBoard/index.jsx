import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ActionButton from '../common/ActionButton';
import withRouter from '../../withRouter';
import './style.scss';
import TodoFieldBoard from '../TodoFieldBoard';

function TodoNewBoard(props) {
  const [focused, setFocused] = useState(false);
  const {
    todoBoard,
    id,
    changeBoardTitle,
    navigate,
    removeBoard,
  } = props;

  const removeBoardWrapper = (e) => {
    e.stopPropagation();

    removeBoard(id);
  };

  const changeFocus = (e) => {
    setFocused(!focused);
    e?.stopPropagation();
  };

  const navigateToBoard = () => {
    navigate(`/board/${id}`);
  };

  return (
    <div
      className="board"
      onClick={navigateToBoard}
    >
      <div>{todoBoard}</div>
      <div
        className="case-button"
        onClick={(e) => { e.stopPropagation(); }}
      >
        <ActionButton
          className="change-title"
          onClick={changeFocus}
        />
        <ActionButton
          className="delete-board"
          onClick={removeBoardWrapper}
        />
      </div>
      {
        focused ? (
          <TodoFieldBoard
            additionalTitle={todoBoard}
            switchMode={changeFocus}
            changeBoardTitle={changeBoardTitle}
            id={id}
            changeFocus={changeFocus}
          />
        ) : null
      }
    </div>
  );
}

TodoNewBoard.propTypes = {
  todoBoard: PropTypes.string.isRequired,
  removeBoard: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  changeBoardTitle: PropTypes.func.isRequired,
  navigate: PropTypes.func.isRequired,
};

export default withRouter(TodoNewBoard);
