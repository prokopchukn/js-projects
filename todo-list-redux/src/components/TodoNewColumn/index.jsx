import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import withRouter from '../../withRouter';

import ActionButton from '../common/ActionButton';
import TodoField from '../TodoField';
import TodoNewItem from '../TodoNewItem';
import {
  fetchItems,
  createItems,
  deleteItems,
  updateItems,
} from '../../store/asyncActions/itemAsyncActions';

import './style.scss';

function TodoNewColumn(props) {
  const dispatch = useDispatch();
  const todoItems = useSelector((state) => state.itemReducer.todoItems);

  const [focused, setFocused] = useState(false);
  const [clicked, setClicked] = useState(false);

  const {
    changeColumnTitle,
    id,
    removeColumn,
    todoColumn,
    buttonTitle,
  } = props;

  useEffect(() => {
    dispatch(fetchItems());
  }, []);

  const changeFocusColumnTitle = () => {
    setFocused(!focused);
  };

  const changeColumnTitleWrapper = (columnTitle) => {
    changeColumnTitle(id, columnTitle);

    setFocused(!focused);
  };

  const removeColumnWrapper = () => {
    removeColumn(id);
  };

  const addNewItem = () => {
    setClicked(!clicked);
  };

  const addNewItems = (fieldValue) => {
    dispatch(createItems(fieldValue, id));
  };

  const removeItem = (idItem) => {
    dispatch(deleteItems(idItem));
  };

  const changeItemTitle = (idItem, fieldValue) => {
    dispatch(updateItems(idItem, fieldValue));
  };

  const columnItems = todoItems.filter((todoItem) => todoItem['todo-columnId'] === id);

  return (
    <div className="container todo-container-list">
      {
        focused ? (
          <TodoField
            title="Save"
            switchMode={changeFocusColumnTitle}
            onSubmit={changeColumnTitleWrapper}
            additionalTitle={todoColumn}
          />
        ) : (
          <>
            <div
              onDoubleClick={changeFocusColumnTitle}
              className="todo-column-title"
            >
              {todoColumn}
            </div>
            <ActionButton
              className="delete-column"
              onClick={removeColumnWrapper}
            />
          </>
        )
      }
      {
        columnItems.map((todoItem) => (
          <TodoNewItem
            key={todoItem.id}
            id={todoItem.id}
            todoItem={todoItem.title}
            removeItem={removeItem}
            changeItemTitle={changeItemTitle}
          />
        ))
      }
      {
        clicked ? (
          <TodoField
            title="Add card"
            isTextarea
            switchMode={addNewItem}
            onSubmit={addNewItems}
          />
        ) : (
          <div>
            <ActionButton
              className="todo-column-item"
              onClick={addNewItem}
              title={buttonTitle}
            />
          </div>
        )
      }
    </div>
  );
}

TodoNewColumn.propTypes = {
  id: PropTypes.number.isRequired,
  removeColumn: PropTypes.func.isRequired,
  changeColumnTitle: PropTypes.func.isRequired,
  todoColumn: PropTypes.string.isRequired,
  buttonTitle: PropTypes.string.isRequired,
};

export default withRouter(TodoNewColumn);
