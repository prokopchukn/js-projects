import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import ActionButton from '../common/ActionButton';

import './style.scss';

function TodoField(props) {
  const [fieldValue, setFieldValue] = useState('');
  const {
    onSubmit,
    title,
    switchMode,
    isTextarea,
  } = props;

  const requestData = async () => {
    const {
      additionalTitle = '',
    } = props;

    setFieldValue(additionalTitle);
  };

  useEffect(() => {
    requestData();
  }, []);

  const onChange = (e) => {
    setFieldValue(e.target.value);
  };

  const clickTodoButton = () => {
    const trimmedValue = fieldValue.trim();

    if (trimmedValue.length <= 100 && trimmedValue) {
      onSubmit(fieldValue);

      setFieldValue('');
    }
  };

  return (
    <div className="container">
      {
          !isTextarea ? (
            <input
              value={fieldValue}
              onChange={onChange}
              type="text"
              placeholder="Enter list title..."
            />
          ) : (
            <textarea
              value={fieldValue}
              onChange={onChange}
              placeholder="Enter a title for this card..."
            />
          )
        }
      <ActionButton
        onClick={clickTodoButton}
        className="confirm-add-list btn"
        title={title}
      />
      <ActionButton
        onClick={switchMode}
        className="back btn"
        title="Back"
      />
    </div>
  );
}

TodoField.propTypes = {
  additionalTitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  switchMode: PropTypes.func.isRequired,
  isTextarea: PropTypes.bool,
};

export default TodoField;
