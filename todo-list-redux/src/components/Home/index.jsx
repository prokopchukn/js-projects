import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import TodoBoardsCreator from '../TodoBoardsCreator';
import TodoNewBoard from '../TodoNewBoard';
import {
  fetchBoards,
  createBoards,
  deleteBoards,
  updateBoards,
} from '../../store/asyncActions/boardAsyncActions';

import './style.scss';

function Home() {
  const dispatch = useDispatch();
  const todoBoards = useSelector((state) => state.boardReducer.todoBoards);

  useEffect(() => {
    dispatch(fetchBoards());
  }, []);

  const addNewBoard = (titleBoard) => {
    dispatch(createBoards(titleBoard));
  };

  const removeBoard = (idBoard) => {
    dispatch(deleteBoards(idBoard));
  };

  const changeBoardTitle = (idBoard, titleBoard) => {
    dispatch(updateBoards(idBoard, titleBoard));
  };

  return (
    <div className="home-container">
      <h1>To do boards</h1>
      <div className="container-boards">
        {
          todoBoards.map((todoBoard) => (
            <TodoNewBoard
              key={todoBoard.id}
              id={todoBoard.id}
              todoBoard={todoBoard.title}
              removeBoard={removeBoard}
              changeBoardTitle={changeBoardTitle}
            />

          ))
        }
        <TodoBoardsCreator
          addNewBoard={addNewBoard}
        />
      </div>
    </div>
  );
}
export default Home;
