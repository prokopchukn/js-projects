import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

function ActionButton(props) {
  const {
    className,
    onClick,
    title,
  } = props;

  return (
    <button
      type="button"
      onClick={onClick}
      className={className}
    >
      {title}
    </button>
  );
}

ActionButton.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string.isRequired,
  title: PropTypes.string,
};

export default ActionButton;
