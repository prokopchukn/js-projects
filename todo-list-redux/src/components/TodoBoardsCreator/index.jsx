import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ActionButton from '../common/ActionButton';
import './style.scss';

function TodoBoardsCreator(props) {
  const [focused, setFocused] = useState(false);
  const [titleBoard, setTitleBoard] = useState('');

  const onChange = (e) => {
    setTitleBoard(e.target.value);
  };

  const changeFocus = () => {
    setFocused(!focused);
  };

  const addNewBoardWrapper = () => {
    const {
      addNewBoard,
    } = props;

    const trimmedValue = titleBoard.trim();

    if (trimmedValue.length <= 100 && trimmedValue) {
      addNewBoard(titleBoard);

      setTitleBoard('');

      changeFocus();
    }
  };

  return (
    <div>
      <div
        className="create-board"
        onClick={changeFocus}
      >
        <div>Create new Board</div>
      </div>
      {
          focused ? (
            <div className="title-board">
              <div>Board title:</div>
              <input
                value={titleBoard}
                onChange={onChange}
                placeholder="Enter the title..."
              />
              <ActionButton
                className="create"
                title="Create"
                onClick={addNewBoardWrapper}
              />
            </div>
          ) : null
        }
    </div>
  );
}

TodoBoardsCreator.propTypes = {
  addNewBoard: PropTypes.func.isRequired,
};

export default TodoBoardsCreator;
