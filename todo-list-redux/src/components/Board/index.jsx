import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import withRouter from '../../withRouter';
import TodoNewColumn from '../TodoNewColumn';
import TodoColumnCreator from '../TodoColumnCreator';
import {
  fetchColumns,
  createColumns,
  deleteColumns,
  updateColumns,
} from '../../store/asyncActions/columnAsyncActions';

import './style.scss';

function Board(props) {
  const dispatch = useDispatch();
  const todoColumns = useSelector((state) => state.columnReducer.todoColumns);

  const { params } = props;

  useEffect(() => {
    dispatch(fetchColumns(params));
  }, []);

  const addNewColumn = (fieldValue) => {
    dispatch(createColumns(fieldValue, params));
  };

  const changeColumnTitle = (idColumn, fieldValue) => {
    dispatch(updateColumns(idColumn, fieldValue));
  };

  const removeColumn = (idColumn) => {
    dispatch(deleteColumns(idColumn));
  };

  return (
    <>
      <div className="return-home">
        <Link to="/">Home</Link>
      </div>

      <div className="todo-column-list">
        {
          todoColumns.map((todoColumn) => (
            <TodoNewColumn
              key={todoColumn.id}
              id={todoColumn.id}
              changeColumnTitle={changeColumnTitle}
              todoColumn={todoColumn.title}
              removeColumn={removeColumn}
              buttonTitle="+ Add a card"
            />
          ))
        }
        <TodoColumnCreator
          onSubmit={addNewColumn}
        />
      </div>
    </>
  );
}

Board.propTypes = {
  params: PropTypes.object,
};

export default withRouter(Board);
