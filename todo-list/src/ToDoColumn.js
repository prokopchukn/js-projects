class ToDoColumn {
  addTodoItem(inputValue, columnId) {
    let isValid = false;
    
    if(inputValue.trim()) {
      isValid = true;
    }

    if(isValid){
      const todoContainerList = document.getElementsByClassName('todo-container-list')[columnId];
      const textareaRefs = todoContainerList.lastElementChild.querySelector('textarea')
      
      const newItemId = document.getElementsByClassName('container-item').length;
  
      todoContainerList.lastElementChild.before(todoItemContainer.render({
        itemTitle: textareaRefs.value,
        isTodoItem: true,
        columnId,
        itemId: newItemId
      }));

      textareaRefs.value = '';
    }
  }

  switchMode = (element, isCreatingMOde) => {
    if (isCreatingMOde) {
      element.hidden = true;
      element.nextElementSibling.hidden = false;
      element.parentElement.nextElementSibling.hidden = true;
    } else {
      element.parentElement.hidden = true;
      element.parentElement.previousElementSibling.hidden = false;
      element.parentElement.parentElement.nextElementSibling.hidden = false;
    }
  }

  editTodoTitle = (inputValue, columnId) => {
    const todoContainerList  = document.getElementsByClassName('todo-container-list')[columnId];
    const title = todoContainerList.getElementsByClassName('todo-column-title')[0];
    title.textContent = inputValue;
    
    this.switchMode(title.nextElementSibling.lastElementChild, false);
  }

  render(props) {
    const {
      title,
      isTodoItem,
      id,
      switchMode,
      buttonTitle,
      onClick
    } = props;

    const titleColumn = $(
      'div',
      {
        class: 'todo-column-title'
      },
      [title[id].value]
    );

    const todoTitleContainer = $(
      'div',
      null,
      [
        titleColumn,
        newListField.render({
          columnId: id,
          isTodoItem,
          buttonTitle: 'Save',
          switchMode: this.switchMode,
          onClick: this.editTodoTitle
        })
      ]
    );
    
    const buttonDeleteColumn = $('div', {class: 'delete-column'});

    const buttonAddTodoItem = newListContainer.render({
      isTodoItem: true,
      columnId: id,
      buttonTitle: 'Add card',
      onClick: this.addTodoItem
    });

    buttonAddTodoItem.firstElementChild.textContent = '+ Add a card';

    const container = $(
      'div', 
      {
        class: ['container', 'todo-container-list']
      },
      [
        todoTitleContainer, 
        buttonDeleteColumn,
        buttonAddTodoItem
      ]
    );

    titleColumn.addEventListener('dblclick', e => {
      this.switchMode(e.target, true);
      e.target.nextElementSibling.children[0].value = e.target.textContent;
    })

    buttonDeleteColumn.addEventListener('click', e => {
      container.hidden = true;
    })

    return container;
  }
}

const toDoColumn = new ToDoColumn();