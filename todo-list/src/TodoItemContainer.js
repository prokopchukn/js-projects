class TodoItemContainer {
  switchMode = (element, isCreatingMOde) => {
    if (isCreatingMOde) {
      element.hidden = true;
      element.nextElementSibling.hidden = false
    } else {
      element.parentElement.hidden = true;
      element.parentElement.previousElementSibling.hidden = false;
    }
  }

  editTodoItem = (inputValue, columnId, itemId) => {
    const item = document.getElementsByClassName('container-item')[itemId];
    item.firstChild.replaceWith(inputValue);
    
    this.switchMode(item.nextElementSibling.lastElementChild, false);
  }

  render(props) {
    const {
      isTodoItem,
      columnId,
      itemTitle,
      itemId
    } = props;

    const container = $('div', null, [
      toDoItem.render({switchMode: this.switchMode, itemTitle}),
      newListField.render({
        columnId,
        isTodoItem,
        buttonTitle: 'Save',
        itemId,
        onClick: this.editTodoItem,
        switchMode: this.switchMode
      })
    ]);

    return container
  }
}

const todoItemContainer = new TodoItemContainer();