class App {
  addTodoColumn(inputValue) {
    const inputRef = document.querySelectorAll('input');

    let isValid = false;
    
    if(inputValue.trim()) {
      isValid = true;
    }

    if(isValid) {
      const newColumnId = document.querySelectorAll(".todo-container-list").length;
      const todoColumnCreator = document.getElementsByClassName('todo-column-list')[0].lastElementChild;

      todoColumnCreator.before(toDoColumn.render({title: inputRef, id: newColumnId}));
      inputRef[newColumnId].value = '';
    }
  }

  render() {
    const container = $(
      'div',
      {
        class: 'todo-column-list'
      }, 
      [
        newListContainer.render({
          onClick: this.addTodoColumn,
          isTodoItem: false,
          buttonTitle: 'Add list'
        })
      ]
    );
    
    return container;
  }
}

const app = new App();