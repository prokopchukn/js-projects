class NewListField {
  constructor() {
    this.state = {
      inputValue: ''
    }
  }

  renderField(isTodoItem) {
    if(!isTodoItem) {
      const titleInput = $(
        'input',
        {
        type: 'text',
        placeholder: 'Enter list title...'
        }
      );

      return titleInput;
    }

    const titleTextArea = $(
      'textarea', 
      {
      placeholder: 'Enter a title for this card...'
      }
    );

    return titleTextArea;
  }

  renderButton(columnId, onClick, buttonTitle, itemId) {
    const buttonAdd = $(
      'button', 
      {
        class: ['confirm-add-list', 'btn']
      }, 
      [buttonTitle]
    );

    buttonAdd.addEventListener('click', e => {
      onClick(this.state.inputValue, columnId, itemId);
      this.state.inputValue = '';
    })

    return buttonAdd;
  }

  render(props) {
    const {
      switchMode,
      isTodoItem,
      columnId,
      onClick,
      buttonTitle,
      itemId
    } = props;

    const field = newListField.renderField(isTodoItem);

    const buttonAdd = newListField.renderButton(columnId, onClick, buttonTitle, itemId);

    const buttonBack = $(
      'button',
      {
        class: ['back', 'btn']
      },
      ['Back']
    );

    const container = $(
      'div', 
      {
        class: 'container'
      },
      [
        field,
        buttonAdd,
        buttonBack
      ]
    );

    container.hidden = true;

    field.addEventListener('input', e => {
      if(e.target.value.length <= 100) {
        this.state.inputValue = e.target.value;
      } else {
        e.target.value = this.state.inputValue;
      }
    })

    buttonBack.addEventListener('click', e => {
      switchMode(e.target, false)
    })

    return container 
  }
}

const newListField = new NewListField();