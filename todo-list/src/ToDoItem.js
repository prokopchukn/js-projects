class ToDoItem {
  render(props) {
    const { itemTitle, switchMode } = props;

    const buttonEditItem = $('div', {class: 'edit-item'});
    const buttonDeleteItem = $('div', {class: 'delete-item'});
    const caseButtons = $('div', {class: 'case-buttons'}, [buttonEditItem, buttonDeleteItem]);
    const container = $('div', {class: 'container-item'}, [itemTitle, caseButtons]);

    buttonDeleteItem.addEventListener('click', e => {
      container.remove();
    });

    buttonEditItem.addEventListener('click', e => {
      switchMode(container, true);
      const textArea = e.target.parentElement.parentElement.nextElementSibling.children[0];
      const todoItemTitle = e.target.parentElement.previousSibling;

      textArea.value = todoItemTitle.textContent;
    });

    return container;
  }
}

const toDoItem = new ToDoItem();