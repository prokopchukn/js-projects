class NewListContainer {
  switchMode(element, isCreatingMOde) {
    if (isCreatingMOde) {
      element.hidden = true;
      element.nextElementSibling.hidden = false
    } else {
      element.parentElement.hidden = true;
      element.parentElement.previousElementSibling.hidden = false;
    }
  }

  render(props) {
    const {
      isTodoItem,
      columnId,
      onClick,
      buttonTitle
    } = props;
   
    const container = $(
      'div', 
      null, 
      [
        newListButton.render({ switchMode: this.switchMode }),
        newListField.render({
          onClick,
          switchMode: this.switchMode,
          isTodoItem: isTodoItem,
          columnId: columnId,
          buttonTitle
        })
      ]
    );

    return container
  }
}

const newListContainer = new NewListContainer();