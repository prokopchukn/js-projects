class NewListButton {
  render(props) {
    const {switchMode} = props;

    const buttonCreateTodo = $(
      'button', 
      {
        class: 'todo-column-item'
      }, 
      ['+ Add a list']
    );

    buttonCreateTodo.addEventListener('click', (e) => {
      switchMode(e.target, true);
    })

    return buttonCreateTodo;
  }
}

const newListButton = new NewListButton();