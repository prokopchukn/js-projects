function $(tagName, attribute, children = []) {
  const container = document.createElement(tagName);
  container.append(...children);

  if(typeof(attribute) === 'object' && attribute !== null) {
    Object.keys(attribute).forEach((el, index) => {
      const value = Array.isArray(attribute[el])
        ? attribute[el].join(" ")
        : attribute[el];

      container.setAttribute(el, value);
    })
  }
  
  return container;
}